Write-Host "STARTING BDC2 CONFIG BDC DEPLOY."
# Session variables
## Set local path of the mo_config folder##
$STARTUP_PATH = "C:\Workspace\catalogue\"; 
# BDC Service account
$env:DOMAIN_SERVICE_ACCOUNT_USERNAME = "BDC2_SVC";
$env:DOMAIN_SERVICE_ACCOUNT_PASSWORD = "_gxy2q5nyLPE<xER";
# BDC administrator account
$env:AZDATA_USERNAME = "bdcadmin";
$env:AZDATA_PASSWORD = "P@ssw0rd1234";
## BDC Master instance name
$BDC_INSTANCE_NAME = 'bdc2.bdc-sql.dr-foster.lan,31434';
#Start
Set-Location $STARTUP_PATH
# Initiliase base config. Creates default control and bdc json files.
azdata bdc config init --source kubeadm-prod --path JSON\BDC2\ -f
# Apply dfi Mo configuration patch files to the default control and bdc json files.
azdata bdc config patch --path "JSON\BDC2\bdc.json" -p "Patch_Files\BDC2_Patch_Files\BDC2_BDC_Config.json"
azdata bdc config patch --path "JSON\BDC2\control.json" -p "Patch_Files\BDC2_Patch_Files\BDC2_Control_Config.json"
#azdata bdc config add --path "JSON\BDC2\bdc.json" -j "$.spec.resources.master.spec.nodeLabel=bdc-master"
# Deploy BDC.
azdata bdc create -c "JSON\BDC2\" --accept-eula y
#Sleeping for the deployment finish internally.
start-sleep -Seconds 30
# Copy the SQL Agent configuration file to the Master pod to enable SQL Server Agent
kubectl cp MSSQL_Agent_Conf\mssql-custom.conf master-0:/var/opt/mssql/mssql-custom.conf -c mssql-server -n bdc2
# Restart SQL Server instance in the Master pod for the SQL Agent configuration to take effect
kubectl -n bdc2 exec master-0 -c mssql-server -- bash -c 'supervisorctl restart mssql-server'
#Sleeping for the restart to finish.
start-sleep -Seconds 60
#Check SQLServer Module Exists Before We Take AD Action.
if (Get-Module -ListAvailable -Name SqlServer) {
   Import-Module -Name SqlServer -Force
} 
else {
    Install-Module -Name SqlServer -Repository DFIGallery -Force
}
# Create dfi_dba database
Invoke-SQLCMD -inputfile 'SQL_Files\BDC2_SQL\bdc_dfi_dba.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "bdcadmin" -Password "$env:AZDATA_PASSWORD"  -QueryTimeout 0 -ConnectionTimeout 0
#Configure SQL Instance Settings
Invoke-SQLCMD -inputfile 'SQL_Files\BDC2_SQL\bdc_sadio_default_settings.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "bdcadmin" -Password "$env:AZDATA_PASSWORD" -QueryTimeout 0 -ConnectionTimeout 0 
# Restart SQL Server instance in the Master pod for the SQL configuration to take effect
kubectl -n bdc2 exec master-0 -c mssql-server -- bash -c 'supervisorctl restart mssql-server'
if (Get-Module -ListAvailable -Name dbatools) {
   Import-Module -Name dbatools -Force
   Update-dbatools
} 
else {
    Install-Module dbatools -RequiredVersion "1.0.115" -Force -Repository DFIGallery
}
$Username = $env:AZDATA_USERNAME
$Password = $env:AZDATA_PASSWORD
$SecurePassword = ConvertTo-SecureString $Password -AsPlainText -Force
$Credential = New-Object System.Management.Automation.PSCredential ($USERNAME,$SecurePassword)
Install-DbaMaintenanceSolution -sqlinstance $BDC_INSTANCE_NAME -Database DFI_DBA -LogToTable -Force -SqlCredential $Credential
Invoke-SQLCMD -inputfile 'SQL_Files\Sadio_SQL\DatabaseBackup_ALL_DATABASES_FULL.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "$env:AZDATA_USERNAME" -Password "$env:AZDATA_PASSWORD"
Invoke-SQLCMD -inputfile 'SQL_Files\Sadio_SQL\DatabaseIntegrityCheck_ALL_DATABASES.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "$env:AZDATA_USERNAME" -Password "$env:AZDATA_PASSWORD"
Invoke-SQLCMD -inputfile 'SQL_Files\Sadio_SQL\IndexOptimize_ALL_DATABASES.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "$env:AZDATA_USERNAME" -Password "$env:AZDATA_PASSWORD"
Install-DbaWhoIsActive -sqlinstance $BDC_INSTANCE_NAME -Database DFI_DBA -Force -SqlCredential $Credential
Install-DbaFirstResponderKit -sqlinstance $BDC_INSTANCE_NAME -Database DFI_DBA -Force -SqlCredential $Credential 
start-sleep -Seconds 15
Write-Host "BDC2 CONFIG BDC HAS BEEN FINISHED."
﻿write-host "This will Uninstall the Sadio BDC in the namespace bdc2."
$confirmation = Read-Host "Are you Sure You Want To Proceed?"
if ($confirmation -eq 'y') {
Write-Host "Uninstalling BDC2 BDC."
#azdata login --auth basic --username bdcadmin --endpoint https://bdc2.bdc-controller.dr-foster.lan:30081
azdata bdc delete --name bdc2 --force

#Check AD Module Exists Before We Take AD Action.
if (Get-Module -ListAvailable -Name activedirectory) {
   Import-Module -Name activedirectory
} 
else {
    Install-Module -Name activedirectory -Repository DFIGallery
}

# We Use The Mo BDC Credentials To Administrate the BDC1 OU.
$UserName = "BDC2_SVC"
$Password = "_gxy2q5nyLPE<xER"
$SecurePassword = ConvertTo-SecureString $Password -AsPlainText -Force
$Credential = New-Object System.Management.Automation.PSCredential ($Username,$SecurePassword)
$BDC_OU_Name = "BDC2"

# Set BDC OU string.
$OU = Get-ADOrganizationalUnit -Filter 'Name -eq $BDC_OU_Name'

# readable list of BDC AD Users.
#Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" | select Name | sort Name

# filter all users in OU minus BDC SVC and BDC User.
$BDCAccounts = Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" | ? {($_.Name -ne 'BDC2 SVC') -and ($_.Name -ne 'BDC User')}

# loop through BDCAccounts.
foreach ($Account in $BDCAccounts)
{
    try
    {
        # Get user and pass to remove
        # remove whatif to execute
        Get-ADUser -Identity $($Account.SamAccountName) | Remove-ADUser -Server DC1.dr-foster.lan -confirm:$false -Credential $Credential -Verbose
        Get-ADUser -Identity $($Account.SamAccountName) | Remove-ADUser -Server DC2.dr-foster.lan -confirm:$false -Credential $Credential -Verbose
        Get-ADUser -Identity $($Account.SamAccountName) | Remove-ADUser -Server DC3.dr-foster.lan -confirm:$false -Credential $Credential -Verbose
        Get-ADUser -Identity $($Account.SamAccountName) | Remove-ADUser -Server nottdc01.dr-foster.lan -confirm:$false -Credential $Credential -Verbose
        Get-ADUser -Identity $($Account.SamAccountName) | Remove-ADUser -Server lhcdc01.dr-foster.lan -confirm:$false -Credential $Credential -Verbose
    }
    catch
    {
        $Error[0]
        Write-Warning "$_"
        Continue
    }
}
}
ElseIf ($confirmation -eq 'n')
{
EXIT
}

# Wait For AD To Replicate Before Reporting AD Clear. Cycle Through All The DC's And Report When All Clear.

$DomainControllers = "DC1","DC2","DC3","nottdc01","lhcdc01"
ForEach ($DC in $DomainControllers)
{
DO
{
$BDC_OU_Name = "BDC2"
$OU = Get-ADOrganizationalUnit -Filter 'Name -eq $BDC_OU_Name'
$Count = (Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" -Server $DC | ? {($_.Name -ne 'BDC2 SVC') -and ($_.Name -ne 'BDC User')}).Count
Write-Host "$DC Count: $Count"
Start-Sleep -Seconds 5
} Until ($Count -eq 0)

}
Write-Host "BDC2 Big Data Cluster Has Been Uninstalled"
Write-Host "STARTING UDL BDC DEPLOY."
# Session variables
## Set local path of the mo_config folder##
$STARTUP_PATH = "C:\Workspace\bdc_deployment_catalogue\catalogue"; 
# BDC Service account
$env:DOMAIN_SERVICE_ACCOUNT_USERNAME = "BDC1_SVC";
$env:DOMAIN_SERVICE_ACCOUNT_PASSWORD = "_gxy2q5nyLPE<xER";
# BDC administrator account
$env:AZDATA_USERNAME = "bdcadmin";
$env:AZDATA_PASSWORD = "tVN87KJFS2g8ht6g";
## BDC Master instance name
$BDC_INSTANCE_NAME = 'udl.dr-foster.lan,31433';
#Start
Set-Location $STARTUP_PATH
# Initiliase base config. Creates default control and bdc json files.
azdata bdc config init --source kubeadm-prod --path JSON\UDL\ -f
# Apply dfi Mo configuration patch files to the default control and bdc json files.
azdata bdc config patch --path "JSON\UDL\bdc.json" -p "Patch_Files\UDL_Patch_Files\UDL_BDC_Config.json"
azdata bdc config patch --path "JSON\UDL\control.json" -p "Patch_Files\UDL_Patch_Files\UDL_Control_Config.json"
# Deploy BDC.
azdata bdc create -c "JSON\UDL\" --accept-eula y
#Sleeping for the deployment finish internally.
start-sleep -Seconds 30
# Copy the SQL Agent configuration file to the Master pod to enable SQL Server Agent
kubectl cp MSSQL_Agent_Conf\mssql-custom.conf master-0:/var/opt/mssql/mssql-custom.conf -c mssql-server -n bdc1
# Restart SQL Server instance in the Master pod for the SQL Agent configuration to take effect
kubectl -n bdc1 exec master-0 -c mssql-server -- bash -c 'supervisorctl restart mssql-server'
#Sleeping for the restart to finish.
#Check SQLServer Module Exists Before We Take AD Action.
if (Get-Module -ListAvailable -Name SqlServer) {
   Import-Module -Name SqlServer -Force
} 
else {
    Install-Module -Name SqlServer -Repository DFIGallery -Force
}
#Set-Location $STARTUP_PATH
# Create dfi_dba database
Invoke-SQLCMD -inputfile 'SQL_Files\UDL_SQL\bdc_dfi_dba.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "$env:AZDATA_USERNAME" -Password "$env:AZDATA_PASSWORD"
#Configure SQL Instance Settings
Invoke-SQLCMD -inputfile 'SQL_Files\UDL_SQL\bdc_mo_default_settings.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "$env:AZDATA_USERNAME" -Password "$env:AZDATA_PASSWORD"
# Restart SQL Server instance in the Master pod for the SQL configuration to take effect
kubectl -n bdc1 exec master-0 -c mssql-server -- bash -c 'supervisorctl restart mssql-server'
if (Get-Module -ListAvailable -Name dbatools) {
   Import-Module -Name dbatools -Force
   Update-dbatools
} 
else {
    Install-Module dbatools -RequiredVersion "1.0.115" -Force -Repository DFIGallery
}
$Username = $env:AZDATA_USERNAME
$Password = $env:AZDATA_PASSWORD
$SecurePassword = ConvertTo-SecureString $Password -AsPlainText -Force
$Credential = New-Object System.Management.Automation.PSCredential ($USERNAME,$SecurePassword)
Install-DbaMaintenanceSolution -sqlinstance $BDC_INSTANCE_NAME -Database DFI_DBA -LogToTable -Force -SqlCredential $Credential
Invoke-SQLCMD -inputfile 'SQL_Files\UDL_SQL\DatabaseBackup_ALL_DATABASES_FULL.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "$env:AZDATA_USERNAME" -Password "$env:AZDATA_PASSWORD"
Invoke-SQLCMD -inputfile 'SQL_Files\UDL_SQL\DatabaseBackup_USER_DATABASES_DIFF.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "$env:AZDATA_USERNAME" -Password "$env:AZDATA_PASSWORD"
Invoke-SQLCMD -inputfile 'SQL_Files\UDL_SQL\DatabaseIntegrityCheck_ALL_DATABASES.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "$env:AZDATA_USERNAME" -Password "$env:AZDATA_PASSWORD"
Invoke-SQLCMD -inputfile 'SQL_Files\UDL_SQL\IndexOptimize_ALL_DATABASES.sql' -ServerInstance $BDC_INSTANCE_NAME -Username "$env:AZDATA_USERNAME" -Password "$env:AZDATA_PASSWORD"
Install-DbaWhoIsActive -sqlinstance $BDC_INSTANCE_NAME -Database DFI_DBA -Force -SqlCredential $Credential
Install-DbaFirstResponderKit -sqlinstance $BDC_INSTANCE_NAME -Database DFI_DBA -Force -SqlCredential $Credential  
start-sleep -Seconds 15
Write-Host "UDL BDC HAS BEEN FINISHED."

#Create Backup Folders
kubectl -n bdc1 exec master-0 -c mssql-server -- bash -c 'mkdir /var/opt/mssql/backups'
kubectl -n bdc1 exec master-0 -c mssql-server -- bash -c 'mkdir /var/opt/mssql/backups/Insight_Layer'
kubectl -n bdc1 exec master-0 -c mssql-server -- bash -c 'mkdir /var/opt/mssql/backups/Supporting_Infomation'
#Grant Permissions On Folder
kubectl -n bdc1 exec master-0 -c mssql-server -- bash -c 'chmod 777 /var/opt/mssql/backups'
kubectl -n bdc1 exec master-0 -c mssql-server -- bash -c 'chmod 777 /var/opt/mssql/backups/Insight_Layer'
kubectl -n bdc1 exec master-0 -c mssql-server -- bash -c 'chmod 777 /var/opt/mssql/backups/Supporting_Infomation'

USE [msdb]
GO
EXEC msdb.dbo.sp_update_jobstep @job_id=N'c762b868-8adb-4227-958a-a396c494a6d9', @step_id=1 , 
		@command=N'EXECUTE [dbo].[DatabaseBackup]
@Databases = ''SYSTEM_DATABASES'',
@Directory = N''/var/opt/mssql'',
@BackupType = ''FULL'',
@Verify = ''Y'',
@CleanupTime = ''7'',
@CheckSum = ''Y'',
@LogToTable = ''Y'''
GO
USE [msdb]
GO
DECLARE @schedule_id int
EXEC msdb.dbo.sp_add_jobschedule @job_id=N'c762b868-8adb-4227-958a-a396c494a6d9', @name=N'Daily @ 00:00', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20200715, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, @schedule_id = @schedule_id OUTPUT
select @schedule_id
GO
USE [msdb]
GO
DECLARE @schedule_id int
EXEC msdb.dbo.sp_add_jobschedule @job_id=N'471ec3f2-d3d4-4efb-99c2-0f5fb3a6b1af', @name=N'Daily @ 00:00', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20200715, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, @schedule_id = @schedule_id OUTPUT
select @schedule_id
GO
USE [msdb]
GO
EXEC msdb.dbo.sp_update_jobstep @job_id=N'4b7fff0b-862e-4384-8221-7eadeac2d59e', @step_id=1 , 
		@command=N'EXECUTE [dbo].[DatabaseIntegrityCheck]
@Databases = ''ALL_DATABASES'',
@DataPurity = ''Y'',
@MaxDOP = ''0'',
@DatabaseOrder = ''DATABASE_SIZE_DESC'',
@LogToTable = ''Y'''
GO
USE [msdb]
GO
EXEC msdb.dbo.sp_update_jobstep @job_id=N'f966798b-0cc7-43df-af39-f392c1c32059', @step_id=1 , 
		@command=N'EXECUTE [dbo].[DatabaseIntegrityCheck]
@Databases = ''ALL_DATABASES'',
@DataPurity = ''Y'',
@MaxDOP = ''0'',
@DatabaseOrder = ''DATABASE_SIZE_DESC'',
@LogToTable = ''Y'''
GO
USE [msdb]
GO
EXEC msdb.dbo.sp_update_jobstep @job_id=N'cd956159-746d-41fc-bab6-9cf13f68c95b', @step_id=1 , 
		@command=N'EXECUTE [dbo].[IndexOptimize]
@Databases = ''ALL_DATABASES'',
@FragmentationLow = NULL,
@FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
@FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
@FragmentationLevel1 = 5,
@FragmentationLevel2 = 30,
@UpdateStatistics = ''ALL'',
--@OnlyModifiedStatistics = ''Y'',
@StatisticsSample = ''100'',
@MAXDOP =''0'',
@SortInTempdb = ''Y'',
@DatabaseOrder = ''DATABASE_SIZE_DESC'',
@LogToTable = ''Y'''
GO
USE [msdb]
GO
DECLARE @schedule_id int
EXEC msdb.dbo.sp_add_jobschedule @job_id=N'cd956159-746d-41fc-bab6-9cf13f68c95b', @name=N'Sunday @ 00:00', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20200715, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, @schedule_id = @schedule_id OUTPUT
select @schedule_id
GO
USE [msdb]
GO
DECLARE @schedule_id int
EXEC msdb.dbo.sp_add_jobschedule @job_id=N'cfa53730-4f11-4849-aa86-a7f5192a3e7e', @name=N'Weekly @ 00:00', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20200715, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, @schedule_id = @schedule_id OUTPUT
select @schedule_id
GO
USE [msdb]
GO
DECLARE @schedule_id int
EXEC msdb.dbo.sp_add_jobschedule @job_id=N'8c55a0ef-f843-491a-82ee-4bec7f06f82f', @name=N'Weekly @ 00:00', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20200715, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, @schedule_id = @schedule_id OUTPUT
select @schedule_id
GO

USE [msdb]
GO
DECLARE @schedule_id int
EXEC msdb.dbo.sp_add_jobschedule @job_id=N'90d6445d-e498-46df-904e-d9bc0ef8488b', @name=N'Daily @ 00:00', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20200715, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, @schedule_id = @schedule_id OUTPUT
select @schedule_id
GO

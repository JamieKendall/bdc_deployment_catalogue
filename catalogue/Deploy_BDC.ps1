function Show-Menu
{
    param (
        [string]$Title = 'BDC Profile Selector'
    )
    Clear-Host
    Write-Host "================ $Title ================"
    
    Write-Host "1: Press '1' To Deploy A Mo BDC."
    Write-Host "2: Press '2' To Deploy A Sadio BDC."
    Write-Host "Q: Press 'Q' to quit."
}
Show-Menu
$selection = Read-Host "Please make a selection"
 switch ($selection)
 {
     '1' {
CD "C:\Workspace\catalogue\Deploy\Mo\"
.\Deploy_Mo_BDC.ps1
         } 
     '2' {
CD "C:\Workspace\catalogue\Deploy\Sadio\"
.\Deploy_Sadio_BDC.ps1
     } 
     'q' {
         return
     }
 }
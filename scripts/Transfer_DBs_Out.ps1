﻿kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/Insight_Layer_INT/ \\rewindsm\DBAPublished\BDC1\Insight_Layer_INT -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/Insight_Layer_QBUILD/ \\rewindsm\DBAPublished\BDC1\Insight_Layer_QBUILD -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/InsightLayerReconciliation/ \\rewindsm\DBAPublished\BDC1\InsightLayerReconciliation -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/master/ \\rewindsm\DBAPublished\BDC1\master -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/model/ \\rewindsm\DBAPublished\BDC1\model -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/msdb/ \\rewindsm\DBAPublished\BDC1\msdb -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/ONSDataStore/ \\rewindsm\DBAPublished\BDC1\ONSDataStore -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/PrimaryCareCDW_AX502/ \\rewindsm\DBAPublished\BDC1\PrimaryCareCDW_AX502 -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/ReferenceData/ \\rewindsm\DBAPublished\BDC1\ReferenceData -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/SHMIDataStore/ \\rewindsm\DBAPublished\BDC1\SHMIDataStore -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/Supporting_Information/ \\rewindsm\DBAPublished\BDC1\Supporting_Infomation -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/Supporting_Information_INT/ \\rewindsm\DBAPublished\BDC1\Supporting_Infomation_INT -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/Supporting_Information_QBUILD/ \\rewindsm\DBAPublished\BDC1\Supporting_Infomation_QBUILD -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/SUSDataStore/ \\rewindsm\DBAPublished\BDC1\SUSDataStore -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/UKFD/ \\rewindsm\DBAPublished\BDC1\UKFD -c mssql-server

kubectl cp bdc1/master-0:var/opt/mssql/backups/master-0/UKOrganisations/ \\rewindsm\DBAPublished\BDC1\UKOrganisations -c mssql-server
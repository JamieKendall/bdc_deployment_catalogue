﻿Get-ChildItem –Path "\\rewindsm\DBAMonthly\BDC1*" -Recurse | Where-Object {($_.LastWriteTime -lt (Get-Date).AddDays(-1))} | Remove-Item -Recurse
kubectl -n bdc1 exec master-0 -c mssql-server -- bash -c "find  var/opt/mssql/backups/ -type f -mtime +6 | xargs rm -f"
kubectl cp bdc1/master-0:var/opt/mssql/backups/ \\rewindsm\DBAMonthly\BDC1 -c mssql-server

Get-ChildItem –Path "\\rewindsm\DBAMonthly\BDC2*" -Recurse | Where-Object {($_.LastWriteTime -lt (Get-Date).AddDays(-7))} | Remove-Item -Recurse
kubectl -n bdc2 exec master-0 -c mssql-server -- bash -c "find  var/opt/mssql/data/master-0/ -type f -mtime +7 | xargs rm -f"
kubectl cp bdc2/master-0:var/opt/mssql/data/master-0/ \\rewindsm\DBAMonthly\BDC2 -c mssql-server
import-module activedirectory

# cred
$UserName = "bigdatasvc"
$Password = "_gxy2q5nyLPE<xER"
$SecurePassword = ConvertTo-SecureString $Password -AsPlainText -Force
$Credential = New-Object System.Management.Automation.PSCredential ($Username,$SecurePassword)

# set BDC OU string
$OU = Get-ADOrganizationalUnit -Filter 'Name -eq "Big Data Clusters"'

# readable list of BDC AD Users
Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" | select Name | sort Name

# filter all users in OU minus BDC SVC and BDC User
$BDCAccounts = Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" | ? {($_.Name -ne 'BDC SVC') -and ($_.Name -ne 'BDC User')}

# loop through BDCAccounts
foreach ($Account in $BDCAccounts)
{
    try
    {
        # Get user and pass to remove
        # remove whatif to execute
        Get-ADUser -Identity $($Account.SamAccountName) | Remove-ADUser -Server DC1.dr-foster.lan -confirm:$false -Credential $Credential -Verbose -WhatIf
    }
    catch
    {
        $Error[0]
        Write-Warning "$_"
        Continue
    }
}
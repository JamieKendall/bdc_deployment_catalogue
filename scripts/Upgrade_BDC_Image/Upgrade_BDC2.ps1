﻿####
#BDC Upgrade Script - BDC2 Namespace
###
#1. Login and check for sessions, abort if found.
#2. Login and check for batches, about if found.
#3. Backup hdfs.
#4. Back up SQL Server master instance.
#5. Update azdata.
#6. Update the Big Data Cluster.

#1. Login and check for sessions, abort if found.

#2. Login and check for batches, about if found.

#3. Backup hdfs.
#azdata bdc hdfs cp --from-path hdfs://user/hive/warehouse/%%D --to-path ./%%D

#4. Back up SQL Server master instance.
if (Get-Module -ListAvailable -Name dbatools) {
   Import-Module -Name dbatools -Force
   Update-dbatools
} 
else {
    Install-Module dbatools -RequiredVersion "1.0.115" -Force -Repository DFIGallery
}
Start-DbaAgentJob -SqlInstance "bdc2.bdc-sql.dr-foster.lan,31434" -Job "DatabaseBackup - ALL_DATABASES - FULL" -SqlCredential bdcadmin

#5. Update azdata.
Write-Output "Uninstalling azdata and installing the latest azdata version."
#Download and Run MSI package for Automated install
$uri = "https://aka.ms/azdata-msi"
$out = "C:\Users\JKendall\AppData\Local\Temp\azdata.msi"
Invoke-WebRequest -uri $uri -OutFile $out
Uninstall-Package -Name "Azure Data CLI" -Force

Start-Process -FilePath "C:\Users\JKendall\AppData\Local\Temp\azdata.msi" -ArgumentList "/passive /norestart"

#6. Update the Big Data Cluster.
azdata login --auth basic --username bdcadmin --endpoint https://bdc2.bdc-controller.dr-foster.lan:30081
azdata bdc status show
azdata bdc upgrade -n bdc1 -t 2019-CU9-ubuntu-16.04 -r mcr.microsoft.com/mssql/bdc

azdata bdc upgrade --n bdc1 --t 2019-CU9-ubuntu-16.04 --r mcr.microsoft.com/mssql/bdc --controller-timeout=30 --component-timeout=40 --stability-threshold=3 --debug
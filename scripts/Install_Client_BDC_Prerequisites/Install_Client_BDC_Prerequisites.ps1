﻿$ErrorActionPreference= 'silentlycontinue'
Set-ExecutionPolicy Unrestricted -Force | Out-Null
###
### BDC Prerequisites
###
### Install Choco
Write-Output "Checking if Choco is installed."
$testchoco = choco -v | Out-Null
if(-not($testchoco)){
    Write-Output "Seems Chocolatey is not installed, so installing now."
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}
else{
    Write-Output "Chocolatey Version $testchoco is already installed. Moving To Next Step."
}
### Install Python
Write-Output "Checking if Python is installed."
$testpython = Python --version | Out-Null
if(-not($testpython)){
    Write-Output "Seems Python is not installed, installing Python."
    choco install python --version=3.6.6 -y
}
else{
    Write-Output "Python Version $testpython is already installed. Moving To Next Step."
}

### Install kubectl
Write-Output "Checking if kubectl is installed."
$testkubectl = kubectl version | Out-Null
if(-not($testkubectl)){
    Write-Output "Seems kubectl is not installed, installing kubectl."
    choco install kubernetes-cli -y
}
else{
    Write-Output "kubectl is already installed. Moving To Next Step."
}
### Install azdata
Write-Output "Checking if azdata is installed."
$testazdata = azdata -v | Out-Null
if(-not($testazdata)){
    Write-Output "Seems azdata is not installed, installing azdata."
    #Download and Run MSI package for Automated install
    $uri = "https://aka.ms/azdata-msi"
    $out = "C:\Users\JKendall\AppData\Local\Temp\azdata.msi"
    Invoke-WebRequest -uri $uri -OutFile $out
    Start-Process -FilePath "C:\Users\JKendall\AppData\Local\Temp\azdata.msi" -ArgumentList "/passive /norestart"
    
}
else{
    Write-Output "azdata is already installed. Moving To Next Step."
}
### Install azure data studio
Write-Output "Checking if Azure Data Studio is installed."
$path = "$env:LOCALAPPDATA\Programs\Azure Data Studio"
$testazuredatastudio = test-path $path
if(-not($testazuredatastudio)){
    Write-Output "Seems Azure Data Studio is not installed, installing Azure Data Studio."
    choco install azure-data-studio -y
}
else{
    Write-Output "Azure Data Sudio is already installed. Moving To Next Step."
}
$testkubectl = kubectl version | Out-Null
if(-not($testkubectl)){

$path = "$env:userprofile\Downloads\kubeconf.txt"
### Setting up kubectl ###
Write-Output "Setting up kubectl config. Please login to gangway with your ad credentials. Download your kubeconfig and save it to your downloads folder: $path"
start-sleep -Seconds 15
$IE=new-object -com internetexplorer.application
$IE.navigate2("https://gangway.dr-foster.lan/")
$IE.visible=$true
while ($null -ne $IE.ReadyState) {
  Start-Sleep -Milliseconds 100
}

If(!(test-path "C:\KubeCTLConfig\"))
{
      New-Item -ItemType Directory -Force -Path "C:\KubeCTLConfig\"
}

Write-Output "Checking that kubeconfig.txt has been downloaded to $path"
$testkubectlconfig = test-path $path
While (!($testkubectlconfig)) {
Write-Output "Kubeconfig.txt is NOT in downloads."
Write-Output "Please download your kubeconf.txt to downloads. $path"
Write-Output "Opening Gangway Again. Please Wait."
start-sleep -Seconds 15
$IE=new-object -com internetexplorer.application
$IE.navigate2("https://gangway.dr-foster.lan/")
$IE.visible=$true
while ($null -ne $IE.ReadyState) {
  Start-Sleep -Milliseconds 100
}
$testkubectlconfig = test-path $path
if(($testkubectlconfig))
{
Continue

}
}

If(($testkubectlconfig))
{
  Move-Item -Path "$path" -Destination "C:\KubeCTLConfig"
  Write-Output "Moving to C:\KubeCTLConfig\"
}

[Environment]::SetEnvironmentVariable("KUBECONFIG",$null,"User")
Write-Output "Setting KUBECONFIG Enviroment Variable."
[System.Environment]::SetEnvironmentVariable('KUBECONFIG', 'C:\KubectlConfig\kubeconf.txt', [System.EnvironmentVariableTarget]::User)
Write-Output "KUBECONFIG Enviroment Variable Set."
}
Write-Output "###"
Write-Output "###"
Write-Output "###"
Write-Output "Your System Has Been Configured To Deploy Big Data Clusters. Please Restart Before Deploying."
Write-Output "###"
Write-Output "###"
Write-Output "###"


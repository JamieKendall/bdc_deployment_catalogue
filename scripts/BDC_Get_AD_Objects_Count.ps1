﻿# cred
$UserName = "bigdatasvc"
$Password = "_gxy2q5nyLPE<xER"
$SecurePassword = ConvertTo-SecureString $Password -AsPlainText -Force
$Credential = New-Object System.Management.Automation.PSCredential ($Username,$SecurePassword)
# set BDC OU string
$OU = Get-ADOrganizationalUnit -Filter 'Name -eq "Big Data Clusters"'
# readable list of BDC AD Users
#Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" | select Name | sort Name
# filter all users in OU minus BDC SVC and BDC User
$BDCAccounts = Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" | ? {($_.Name -ne 'BDC SVC') -and ($_.Name -ne 'BDC User')}
(Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" -Server DC1 | ? {($_.Name -ne 'BDC SVC') -and ($_.Name -ne 'BDC User')}).Count
(Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" -Server DC2 | ? {($_.Name -ne 'BDC SVC') -and ($_.Name -ne 'BDC User')}).Count
(Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" -Server DC3 | ? {($_.Name -ne 'BDC SVC') -and ($_.Name -ne 'BDC User')}).Count
(Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" -Server nottdc01 | ? {($_.Name -ne 'BDC SVC') -and ($_.Name -ne 'BDC User')}).Count
(Get-ADUser -Filter * -SearchBase "$($ou.distinguishedname)" -Server lhcdc01 | ? {($_.Name -ne 'BDC SVC') -and ($_.Name -ne 'BDC User')}).Count


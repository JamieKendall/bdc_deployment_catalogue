# session variables
$STARTUP_PATH                           = "F:\git\ops\bdc_deploy\"; # path to local clone of repository
$env:DOMAIN_SERVICE_ACCOUNT_USERNAME    = "bigdatasvc";
$env:DOMAIN_SERVICE_ACCOUNT_PASSWORD    = "_gxy2q5nyLPE<xER";
Set-Location $STARTUP_PATH
### region ends

# initiliase base config. Creates default control and bdc json files
azdata bdc config init --source kubeadm-prod --target bdc_deploy -f

# apply dfi config. Sets specfici configs for DFI
azdata bdc config patch -c bdc_deploy/control.json -p $STARTUP_PATH/scripts/control_config.json
azdata bdc config patch -c bdc_deploy/bdc.json -p $STARTUP_PATH/scripts/bdc_config.json

# create bdc. azdata login first!
azdata bdc create -c bdc_deploy --accept-eula y
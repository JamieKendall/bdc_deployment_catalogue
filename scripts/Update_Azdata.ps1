﻿#Update azdata.
Write-Output "Uninstalling azdata and installing the latest azdata version."
#Download and Run MSI package for Automated install
$uri = "https://aka.ms/azdata-msi"
$out = "C:\Users\JKendall\AppData\Local\Temp\azdata.msi"
Invoke-WebRequest -uri $uri -OutFile $out
Uninstall-Package -Name "Azure Data CLI" -Force

Start-Process -FilePath "C:\Users\JKendall\AppData\Local\Temp\azdata.msi" -ArgumentList "/passive /norestart"